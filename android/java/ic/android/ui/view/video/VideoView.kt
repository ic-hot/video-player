package ic.android.ui.view.video


import android.content.Context
import android.util.AttributeSet

import ic.ifaces.lifecycle.closeable.Closeable



class VideoView : android.widget.VideoView, Closeable {


	override fun onAttachedToWindow() {
		super.onAttachedToWindow()
		start()
	}

	override fun onDetachedFromWindow() {
		super.onDetachedFromWindow()
		if (isPlaying) {
			stopPlayback()
		}
	}


	fun setVideoUrl (videoUrl: String) {
		setVideoPath(videoUrl)
	}


	override fun close() {
		if (isPlaying) {
			stopPlayback()
		}
	}


	@Suppress("ConvertSecondaryConstructorToPrimary")
	@JvmOverloads
	constructor(context: Context, attrs: AttributeSet? = null) : super (context, attrs)


}